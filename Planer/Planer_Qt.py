# -*- coding: utf-8 -*-
__author__ = 'rafix'

from PyQt4 import QtCore, QtGui, QtSql
from datetime import datetime, timedelta
from Planer_db import PlanerDb
import time
import base64

DB_LIST_FILE_PATH = "/home/rafix/db.txt"
PATH = r"/home/rafix/PycharmProjects/python-projects/Planer/Planer_Qt.py"

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8

    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)


class Ui_Planer_login:

    def setupUi(self, Planer):

        Planer.setObjectName(_fromUtf8("Planer"))
        Planer.resize(770, 547)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        Planer.setPalette(palette)

        self.lineEdit_user = QtGui.QLineEdit(Planer)
        self.lineEdit_user.setGeometry(QtCore.QRect(330, 90, 113, 27))

        self.lineEdit_password = QtGui.QLineEdit(Planer)
        self.lineEdit_password.setGeometry(QtCore.QRect(330, 150, 113, 27))
        self.lineEdit_password.setEchoMode(QtGui.QLineEdit.Password)

        self.loginButton = QtGui.QPushButton(Planer)
        self.loginButton.setGeometry(QtCore.QRect(342, 190, 91, 27))
        self.loginButton.clicked.connect(lambda: self.login())

        self.create_accountButton = QtGui.QPushButton(Planer)
        self.create_accountButton.setGeometry(QtCore.QRect(342, 220, 91, 27))
        self.create_accountButton.clicked.connect(lambda: self.create_account()
                                                  )

        self.delete_accountButton = QtGui.QPushButton(Planer)
        self.delete_accountButton.setGeometry(QtCore.QRect(342, 250, 91, 27))
        self.delete_accountButton.clicked.connect(lambda: self.delete_account()
                                                  )

        self.label_User = QtGui.QLabel(Planer)
        self.label_User.setGeometry(QtCore.QRect(330, 70, 121, 17))
        self.label_User.setText(_translate("Planer", "User:", None))

        self.label_Password = QtGui.QLabel(Planer)
        self.label_Password.setGeometry(QtCore.QRect(330, 130, 121, 17))
        self.label_Password.setText(_translate("Planer", "Password:", None))

        self.retranslateUi(Planer)

    def retranslateUi(self, Planer):

        Planer.setWindowTitle(_translate("Planer", "Planer Login", None))
        self.loginButton.setText(
            _translate("Planer", "Login", None))
        self.create_accountButton.setText(_translate("Planer",
                                                     "Create account", None))

        self.delete_accountButton.setText(
            _translate("Planer", "Delete account", None))

    def login(self):

        if self.lineEdit_user.text() == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Username field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return

        elif self.lineEdit_password.text() == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Password field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return

        cur.execute('''SELECT "user", password from {0}
                       WHERE "user" = '{1}';'''
                    .format('admin_table', self.lineEdit_user.text()))
        column = cur.fetchone()

        if column is not None:

            pass_login = self.lineEdit_password.text()
            pass_db = base64.b64decode(column['password'])

            if pass_db == pass_login:

                QtGui.QMessageBox.information(QtGui.QWidget(), 'SUCCESS!!!',
                                              "Login successful!!!",
                                              QtGui.QMessageBox.Ok)
                database.db_swich('db_' + self.lineEdit_user.text())
                Planer.accept()
            else:
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Wrong username or password!!!",
                                              QtGui.QMessageBox.Ok)
        else:
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Wrong username or password!!!",
                                          QtGui.QMessageBox.Ok)

    def create_account(self):

        cur.execute(
            '''SELECT * from {0} WHERE "user" = '{1}';'''.format(
                'admin_table', self.lineEdit_user.text()))
        column = cur.fetchone()
        if column is not None:
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Provided username already in use!!!",
                                          QtGui.QMessageBox.Ok)
        else:
            if self.lineEdit_user.text() == "":
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Username field empty!!!",
                                              QtGui.QMessageBox.Ok)
            elif self.lineEdit_password.text() == "":
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Password field empty!!!",
                                              QtGui.QMessageBox.Ok)
            else:
                password_en = self.password_encode(self.lineEdit_password.text())

                cur.execute('''INSERT INTO {0} ("user", password)
                               VALUES ('{1}', '{2}')'''
                            .format('admin_table', self.lineEdit_user.text(),
                                    password_en))

                database.conn.commit()
                database.db_swich('db_' + self.lineEdit_user.text())

                QtGui.QMessageBox.information(QtGui.QWidget(), 'SUCCESS!',
                                              "Account has been created!!",
                                              QtGui.QMessageBox.Ok)

                self.lineEdit_user.clear()
                self.lineEdit_password.clear()

    def delete_account(self):

        if self.lineEdit_user.text() == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Username field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return
        elif self.lineEdit_password.text() == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Password field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return

        cur.execute('''SELECT "user", password from {0}
                   WHERE "user" = '{1}';'''
                    .format('admin_table', self.lineEdit_user.text()))

        column = cur.fetchone()
        print column

        if column is not None:

            pass_login = self.lineEdit_password.text()
            pass_db = base64.b64decode(column['password'])

            if pass_db == pass_login:

                cur.execute('''DELETE FROM {0}
                               WHERE "user" = '{1}';'''
                            .format('admin_table', self.lineEdit_user.text()))
                database.conn.commit()
                database.db_swich('admin')
                database.db_delete('db_' + self.lineEdit_user.text())
                database.conn.commit()
                db_list = []

                with open(DB_LIST_FILE_PATH, 'r') as list_file:

                    readlines = list_file.readlines()

                    for line in readlines:
                        db_list.append(line.strip())

                with open(DB_LIST_FILE_PATH, 'w+') as list_file:
                    if 'db_' + self.lineEdit_user.text() in db_list:
                        db_list.remove('db_' + self.lineEdit_user.text())
                    for x in db_list:
                        list_file.write(x + '\n')

                QtGui.QMessageBox.information(QtGui.QWidget(), 'SUCCESS!!!',
                                              "Account has been deleted!!!",
                                              QtGui.QMessageBox.Ok)
                self.lineEdit_user.clear()
                self.lineEdit_password.clear()
            else:
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Wrong username or password!!!",
                                              QtGui.QMessageBox.Ok)
        else:
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Wrong username or password!!!",
                                          QtGui.QMessageBox.Ok)

    def password_encode(self, password):
        pass_en = base64.b64encode(str(password))
        return pass_en


class Ui_Planer:

    def setupUi(self, Planer):

        self.lista_1 = []

        Planer.setObjectName(_fromUtf8("Planer"))
        Planer.resize(770, 547)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        Planer.setPalette(palette)

        self.timeEdit = QtGui.QTimeEdit(Planer)
        self.timeEdit.setGeometry(QtCore.QRect(10, 30, 113, 27))
        self.timeEdit.setDisplayFormat("HH:mm")
        self.timeEdit.setTime(QtCore.QTime.currentTime())

        self.lineEdit_2 = QtGui.QLineEdit(Planer)
        self.lineEdit_2.setGeometry(QtCore.QRect(10, 90, 113, 27))
        self.lineEdit_2.setObjectName(_fromUtf8("lineEdit_2"))
        self.lineEdit_3 = QtGui.QLineEdit(Planer)
        self.lineEdit_3.setGeometry(QtCore.QRect(10, 150, 113, 27))
        self.lineEdit_3.setObjectName(_fromUtf8("lineEdit_3"))
        self.lineEdit_4 = QtGui.QLineEdit(Planer)
        self.lineEdit_4.setGeometry(QtCore.QRect(10, 210, 113, 27))
        self.lineEdit_4.setObjectName(_fromUtf8("lineEdit_4"))

        self.comboBox = QtGui.QComboBox(Planer)
        self.comboBox.setGeometry(QtCore.QRect(220, 30, 111, 27))
        self.comboBox.setObjectName(_fromUtf8("comboBox"))
        self.comboBox_2 = QtGui.QComboBox(Planer)
        self.comboBox_2.setGeometry(QtCore.QRect(220, 90, 111, 27))
        self.comboBox_2.setObjectName(_fromUtf8("comboBox_2"))
        self.comboBox_3 = QtGui.QComboBox(Planer)
        self.comboBox_3.setGeometry(QtCore.QRect(220, 150, 111, 27))
        self.comboBox_3.setObjectName(_fromUtf8("comboBox_3"))
        self.comboBox_4 = QtGui.QComboBox(Planer)
        self.comboBox_4.setGeometry(QtCore.QRect(220, 210, 111, 27))
        self.comboBox_4.setObjectName(_fromUtf8("comboBox_4"))
        self.comboBox_5 = QtGui.QComboBox(Planer)
        self.comboBox_5.setGeometry(QtCore.QRect(220, 270, 111, 27))
        self.comboBox_5.setObjectName(_fromUtf8("comboBox_5"))

        cb1 = GetLineEditData(self.timeEdit, self.comboBox, cur, 'hours',
                              'hour')
        cb2 = GetLineEditData(self.lineEdit_2, self.comboBox_2, cur, 'classes',
                              'class')
        cb3 = GetLineEditData(self.lineEdit_3, self.comboBox_3, cur,
                              'classrooms', 'classroom')
        cb4 = GetLineEditData(self.lineEdit_4, self.comboBox_4, cur,
                              'teachers', 'teacher')
        self.week_days = QtCore.QStringList(['Monday', 'Tuesday',
                                             'Wednesday', 'Thursday',
                                             'Friday', 'Saturday',
                                             'Sunday'])

        self.comboBox.addItems(cb1.db_read_column())
        self.comboBox_2.addItems(cb2.db_read_column())
        self.comboBox_3.addItems(cb3.db_read_column())
        self.comboBox_4.addItems(cb4.db_read_column())
        self.comboBox_5.addItems(self.week_days)

        self.toolButton = QtGui.QToolButton(Planer)
        self.toolButton.setGeometry(QtCore.QRect(160, 30, 31, 25))
        self.toolButton.setObjectName(_fromUtf8("toolButton"))
        self.toolButton.clicked.connect(lambda: cb1.le_to_cb(time_flag=True))

        self.toolButton_2 = QtGui.QToolButton(Planer)
        self.toolButton_2.setGeometry(QtCore.QRect(160, 90, 31, 25))
        self.toolButton_2.setObjectName(_fromUtf8("toolButton_2"))
        self.toolButton_2.clicked.connect(lambda: cb2.le_to_cb(
            time_flag=False))

        self.toolButton_3 = QtGui.QToolButton(Planer)
        self.toolButton_3.setGeometry(QtCore.QRect(160, 150, 31, 25))
        self.toolButton_3.setObjectName(_fromUtf8("toolButton_3"))
        self.toolButton_3.clicked.connect(lambda: cb3.le_to_cb(
            time_flag=False))

        self.toolButton_4 = QtGui.QToolButton(Planer)
        self.toolButton_4.setGeometry(QtCore.QRect(160, 210, 31, 25))
        self.toolButton_4.setObjectName(_fromUtf8("toolButton_4"))
        self.toolButton_4.clicked.connect(lambda: cb4.le_to_cb(
            time_flag=False))

        dcb1 = DelPositionFromCb(self.comboBox, cur, 'hours',
                              'hour')

        self.deltoolButton = QtGui.QToolButton(Planer)
        self.deltoolButton.setGeometry(QtCore.QRect(330, 30, 31, 25))
        self.deltoolButton.setObjectName(_fromUtf8("deltoolButton"))
        self.deltoolButton.clicked.connect(lambda: dcb1.del_position())

        self.listWidget = QtGui.QListWidget(Planer)
        self.listWidget.setGeometry(QtCore.QRect(460, 30, 71, 271))
        self.listWidget.setObjectName(_fromUtf8("listWidget"))

        self.listWidget.itemClicked.connect(
            lambda: table.initializeModel(self.listWidget.currentItem().text(),
                                          1))

        self.listWidget2 = QtGui.QListWidget(Planer)
        self.listWidget2.setGeometry(QtCore.QRect(550, 30, 71, 271))
        self.listWidget2.setObjectName(_fromUtf8("listWidget2"))

        self.listWidget2.itemClicked.connect(
            lambda: table.initializeModel(
                self.listWidget2.currentItem().text(),
                2))

        self.listWidget3 = QtGui.QListWidget(Planer)
        self.listWidget3.setGeometry(QtCore.QRect(640, 30, 71, 271))
        self.listWidget3.setObjectName(_fromUtf8("listWidget3"))

        self.listWidget3.itemClicked.connect(
            lambda: table.initializeModel(
                self.listWidget3.currentItem().text(),
                3))

        self.fill_list_widget(self.listWidget, 'classroom')
        self.fill_list_widget(self.listWidget2, 'teacher')
        self.fill_list_widget(self.listWidget3, 'class')

        table.createView()

        self.label = QtGui.QLabel(Planer)
        self.label.setGeometry(QtCore.QRect(10, 10, 261, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.label_2 = QtGui.QLabel(Planer)
        self.label_2.setGeometry(QtCore.QRect(10, 70, 121, 17))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label_3 = QtGui.QLabel(Planer)
        self.label_3.setGeometry(QtCore.QRect(10, 130, 161, 17))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(Planer)
        self.label_4.setGeometry(QtCore.QRect(220, 10, 121, 17))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.label_5 = QtGui.QLabel(Planer)
        self.label_5.setGeometry(QtCore.QRect(220, 70, 101, 17))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(Planer)
        self.label_6.setGeometry(QtCore.QRect(220, 130, 91, 17))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(Planer)
        self.label_7.setGeometry(QtCore.QRect(220, 190, 141, 17))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtGui.QLabel(Planer)
        self.label_8.setGeometry(QtCore.QRect(10, 190, 161, 17))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_9 = QtGui.QLabel(Planer)
        self.label_9.setGeometry(QtCore.QRect(220, 250, 161, 17))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.label_10 = QtGui.QLabel(Planer)
        self.label_10.setGeometry(QtCore.QRect(10, 290, 221, 17))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.label_11 = QtGui.QLabel(Planer)
        self.label_11.setGeometry(QtCore.QRect(460, 10, 55, 17))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.label_13 = QtGui.QLabel(Planer)
        self.label_13.setGeometry(QtCore.QRect(550, 10, 80, 17))
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.label_14 = QtGui.QLabel(Planer)
        self.label_14.setGeometry(QtCore.QRect(640, 10, 80, 17))
        self.label_14.setObjectName(_fromUtf8("label_13"))

        self.label_12 = QtGui.QLabel(Planer)
        self.label_12.setGeometry(QtCore.QRect(550, 420, 221, 17))

        font = QtGui.QFont()
        font.setPointSize(15)
        font.setBold(True)
        font.setWeight(75)

        self.label_12.setFont(font)
        self.label_12.setObjectName(_fromUtf8("label_12"))

        self.pushButton = QtGui.QPushButton(Planer)
        self.pushButton.setGeometry(QtCore.QRect(380, 130, 61, 27))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.pushButton.clicked.connect(self.reserve)

        self.pushButton_3 = QtGui.QPushButton(Planer)
        self.pushButton_3.setGeometry(QtCore.QRect(490, 321, 61, 27))
        self.pushButton_3.setObjectName(_fromUtf8("pushButton_3"))

        self.pushButton_3.clicked.connect(lambda: table.delete_row())

        self.pushButton_4 = QtGui.QPushButton(Planer)
        self.pushButton_4.setGeometry(QtCore.QRect(600, 321, 61, 27))
        self.pushButton_4.setObjectName(_fromUtf8("pushButton_4"))

        self.pushButton_4.clicked.connect(lambda: self.logout())

        self.retranslateUi(Planer)
        QtCore.QMetaObject.connectSlotsByName(Planer)

        table.table_fill()

    def retranslateUi(self, Planer):

        Planer.setWindowTitle(_translate("Planer", "Form", None))
        self.toolButton.setText(_translate("Planer", ">>", None))
        self.toolButton_2.setText(_translate("Planer", ">>", None))
        self.toolButton_3.setText(_translate("Planer", ">>", None))
        self.pushButton.setText(_translate("Planer", ">>>", None))
        self.label.setText(_translate("Planer",
                                      "Insert lesson hour:",
                                      None))
        self.label_2.setText(_translate("Planer", "Insert class:", None))
        self.label_3.setText(_translate("Planer", "Insert classroom:", None)
                             )
        self.label_4.setText(_translate("Planer", "Choice hour", None))
        self.label_5.setText(_translate("Planer", "Choice class", None))
        self.label_6.setText(_translate("Planer", "Choice classroom", None))
        self.label_7.setText(_translate("Planer", "Choice teacher", None))
        self.label_8.setText(_translate("Planer", "Insert teacher", None)
                             )
        self.toolButton_4.setText(_translate("Planer", ">>", None))
        self.label_9.setText(_translate("Planer", "Choice weekday",
                                        None))

        self.label_10.setText(_translate("Planer",
                                         "Table of reservation",
                                         None))
        self.label_11.setText(_translate("Planer", "Classroom", None))
        #self.label_12.setText(_translate("Planer", "Classroom", None))
        self.label_13.setText(_translate("Planer", "Teacher", None))
        self.label_14.setText(_translate("Planer", "Classroom", None))

        self.pushButton_3.setText(_translate("Planer", "Delete", None))
        self.pushButton_4.setText(_translate("Planer", "Logout", None))

        self.deltoolButton.setText(_translate("Planer", "Del", None))

    def reserve(self):
        '''Method which indicates which record fulfill criteria from
            comboboxes'''

        try:
            cur.execute('''SELECT * from {0}
                           WHERE hour = '{1}'
                           AND week_day = '{2}'
                           AND classroom = '{3}';'''.format('reservation',
                        self.comboBox.currentText(),
                        self.comboBox_5.currentText(),
                        self.comboBox_3.currentText()
                        ))
            column = cur.fetchone()

            if column is not None:
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Classroom {0} has already been reserved for {1} at {2} by {3} !!!".format(
                                                  column['classroom'],
                                                  column['week_day'],
                                                  column['hour'],
                                                  column['teacher']),
                                              QtGui.QMessageBox.Ok)
                raise Exception("Alert")

            cur.execute('''SELECT * from {0}
                           WHERE hour = '{1}'
                           AND week_day = '{2}'
                           AND teacher = '{3}';'''.format(
                                                  'reservation',
                                                  self.comboBox.currentText(),
                                                  self.comboBox_5.currentText(),
                                                  self.comboBox_4.currentText()
                                                  ))
            column = cur.fetchone()

            if column is not None:
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Teacher {0} has already classes with class {1} on {2} at {3} !!!".format(
                                                  column['teacher'],
                                                  column['class'],
                                                  column['week_day'],
                                                  column['hour']),
                                              QtGui.QMessageBox.Ok)
                raise Exception("Alert")

            cur.execute('''SELECT * from {0}
                           WHERE class = '{1}'
                           AND hour = '{2}'
                           AND week_day = '{3}';'''.format(
                                                'reservation',
                                                self.comboBox_2.currentText(),
                                                self.comboBox.currentText(),
                                                self.comboBox_5.currentText()
                                                ))
            column = cur.fetchone()

            if column is not None:
                QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                              "Class {0} has already classes on {1} at {2} in classroom {3}!!!".format(
                                                  column['class'],
                                                  column['week_day'],
                                                  column['hour'],
                                                  column['classroom']),
                                              QtGui.QMessageBox.Ok)
                raise Exception("Alert")
        except Exception as e:
            print e
        else:
            self.reservation_insert()
            self.class_table_count = self.listWidget.count()
            print self.class_table_count
            self.fill_list_widget(self.listWidget, 'classroom')
            self.class_table_count2 = self.listWidget.count()
            print self.class_table_count2
            self.fill_list_widget(self.listWidget2, 'teacher')
            self.fill_list_widget(self.listWidget3, 'class')

    def reservation_insert(self):
        '''Method which insert values from comboboxes to database'''

        cur.execute('''INSERT INTO {0} (hour, week_day, class, classroom, teacher)
                        VALUES ('{1}', '{2}', '{3}', '{4}', '{5}');'''.format(
            'reservation',
            self.comboBox.currentText(),
            self.comboBox_5.currentText(),
            self.comboBox_2.currentText(),
            self.comboBox_3.currentText(),
            self.comboBox_4.currentText()
        ))
        database.conn.commit()
        table.table_fill()

    def fill_list_widget(self, lw, list_item, *args):
        '''Metchod which add positions to listWidgets'''

        content = []
        cur.execute('''SELECT {0} FROM {1}'''.format(list_item, 'reservation'))
        column = cur.fetchall()
        for x in column:
            if x[list_item] not in content:
                content.append(x[list_item])
        lw.clear()
        content.sort()
        lw.addItems(content)
        # if list_item not in content:
        #     for x in args:
        #         x.setText(_translate("Planer", "", None))

    def logout(self):
        database.conn.close()
        print "database connection closed"
        table.qtdb.close()
        print "qtdb connection closed"
        Planer.close()
        import subprocess
        subprocess.call("python {0}".format(PATH), shell=True)


class AdminWindow:

    def setupUi(self, Planer):

        Planer.setObjectName(_fromUtf8("Planer"))
        Planer.resize(770, 547)
        palette = QtGui.QPalette()
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Active, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(0, 0, 255))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Inactive, QtGui.QPalette.Text, brush)
        brush = QtGui.QBrush(QtGui.QColor(159, 158, 158))
        brush.setStyle(QtCore.Qt.SolidPattern)
        palette.setBrush(QtGui.QPalette.Disabled, QtGui.QPalette.Text, brush)
        Planer.setPalette(palette)

        self.label_User = QtGui.QLabel(Planer)
        self.label_User.setGeometry(QtCore.QRect(10, 60, 113, 27))
        self.label_User.setText(_translate("Planer", "Old admin password:", None))

        self.label_Password = QtGui.QLabel(Planer)
        self.label_Password.setGeometry(QtCore.QRect(10, 120, 113, 27))
        self.label_Password.setText(_translate("Planer", "New admin password:", None))

        self.lineEdit_oldpassword = QtGui.QLineEdit(Planer)
        self.lineEdit_oldpassword.setGeometry(QtCore.QRect(10, 90, 113, 27))
        self.lineEdit_oldpassword.setObjectName(_fromUtf8("lineEdit_oldpassword"))
        self.lineEdit_oldpassword.setEchoMode(QtGui.QLineEdit.Password)
        self.lineEdit_newpassword = QtGui.QLineEdit(Planer)
        self.lineEdit_newpassword.setGeometry(QtCore.QRect(10, 150, 113, 27))
        self.lineEdit_newpassword.setObjectName(_fromUtf8("lineEdit_newpassword"))
        self.lineEdit_newpassword.setEchoMode(QtGui.QLineEdit.Password)

        self.adminlistWidget = QtGui.QListWidget(Planer)
        self.adminlistWidget.setGeometry(QtCore.QRect(460, 30, 71, 271))
        self.adminlistWidget.setObjectName(_fromUtf8("listWidget"))
        self.fill_list_widget(self.adminlistWidget, "user")

        self.logoutButton = QtGui.QPushButton(Planer)
        self.logoutButton.setGeometry(QtCore.QRect(600, 321, 61, 27))
        self.logoutButton.setObjectName(_fromUtf8("logoutButton"))
        self.logoutButton.setText(_translate("Planer", "Logout", None))

        self.logoutButton.clicked.connect(lambda: self.logout())

        self.deleteButton = QtGui.QPushButton(Planer)
        self.deleteButton.setGeometry(QtCore.QRect(400, 321, 100, 27))
        self.deleteButton.setObjectName(_fromUtf8("logoutButton"))
        self.deleteButton.setText(_translate("Planer", "Delete account", None))

        self.deleteButton.clicked.connect(lambda: self.delete_account())

        self.pushButton_chpwd = QtGui.QPushButton(Planer)
        self.pushButton_chpwd.setGeometry(QtCore.QRect(10, 210, 200, 27))
        self.pushButton_chpwd.setObjectName(_fromUtf8("pushButton_chpwd"))
        self.pushButton_chpwd.setText(_translate("Planer", "Change admin password", None))

        self.pushButton_chpwd.clicked.connect(lambda: self.change_adminpasswd(self.lineEdit_oldpassword.text(), self.lineEdit_newpassword.text()))

    def logout(self):
        database.conn.close()
        print "database connection closed"
        Planer.close()
        import subprocess
        subprocess.call("python {0}".format(PATH), shell=True)

    def fill_list_widget(self, lw, list_item, *args):
        '''Metchod which add positions to listWidgets'''

        content = []
        cur.execute('''SELECT "{0}" FROM {1}'''.format(list_item, 'admin_table'))
        column = cur.fetchall()
        print column
        for x in column:
            if x[list_item] not in content and x[list_item] != 'admin':
                content.append(x[list_item])
        lw.clear()
        content.sort()
        lw.addItems(content)

    def change_adminpasswd(self, old_password, new_password):
        if old_password == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Old password field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return

        elif new_password == "":
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Password field empty!!!",
                                          QtGui.QMessageBox.Ok)
            return
        cur.execute('''SELECT {0} FROM {1} WHERE "user" = '{2}';'''.format('password', 'admin_table', 'admin'))
        column = cur.fetchone()
        db_pass = base64.b64decode(column['password'])

        if db_pass == old_password:
            cur.execute(
                '''UPDATE {0} SET password= '{1}' WHERE "user" = '{2}';'''.format('admin_table', base64.b64encode(str(new_password)), 'admin'))
            database.conn.commit()
            QtGui.QMessageBox.information(QtGui.QWidget(), 'SUCCESS!!!',
                                          "Password for admin user has been changed!!!",
                                          QtGui.QMessageBox.Ok)
        else:
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Bad old password!!!",
                                          QtGui.QMessageBox.Ok)
        self.lineEdit_newpassword.clear()
        self.lineEdit_oldpassword.clear()

    def delete_account(self):

        user = self.adminlistWidget.currentItem()
        if user is None:
            QtGui.QMessageBox.information(QtGui.QWidget(), 'WARNING!!!',
                                          "Please select user for delete!!!",
                                          QtGui.QMessageBox.Ok)
            return

        cur.execute('''DELETE FROM {0}
                           WHERE "user" = '{1}';'''
                    .format('admin_table', user.text()))
        database.conn.commit()
        database.db_swich('admin')
        database.db_delete('db_' + user.text())
        database.conn.commit()
        db_list = []

        with open(DB_LIST_FILE_PATH, 'r') as list_file:

            readlines = list_file.readlines()

            for line in readlines:
                db_list.append(line.strip())

        with open(DB_LIST_FILE_PATH, 'w+') as list_file:
            if 'db_' + user.text() in db_list:
                db_list.remove('db_' + user.text())
            for x in db_list:
                list_file.write(x + '\n')

        QtGui.QMessageBox.information(QtGui.QWidget(), 'SUCCESS!!!',
                                      "Account has been deleted!!!",
                                      QtGui.QMessageBox.Ok)

        self.fill_list_widget(self.adminlistWidget, "user")







class GetLineEditData:
    '''Class which is responsible for addition of values from line edits fields to
    comboboxes and database'''
    def __init__(self, lE, cB, cur, table, column):
        self.lE = lE
        self.cB = cB
        self.cur = cur
        self.table = table
        self.column = column
        self.column_content = []

    def db_read_column(self):
        '''Method which read content of column in db table sort it and
        return '''
        self.cur.execute('SELECT {0} FROM {1}'.format(self.column, self.table))
        for x in self.cur:
            for y in x:
                self.column_content.append(x[y])
        self.column_content.sort()
        return self.column_content

    def le_to_cb(self, time_flag):
        '''Method which add value from line Edit fields to comboboxes if
        there is no duplicates'''

        self.db_read_column()
        if time_flag is True:

            plus_45 = datetime.strptime(str(self.lE.text()), '%H:%M') + timedelta(minutes=45)
            hour = self.lE.text() + " - " + "{:%H:%M}".format(plus_45)


            #if self.lE.text() not in self.column_content:
            if hour not in self.column_content:
                self.cur.execute("INSERT INTO {0} ({1}) VALUES ('{2}')"
                                 .format(self.table, self.column,
                                         hour))
                self.column_content = []
                self.db_read_column()

                self.cB.clear()
                self.cB.addItems(self.column_content)

                database.conn.commit()
                print self.column_content

        else:

            if self.lE.text() not in self.column_content:
                self.cur.execute("INSERT INTO {0} ({1}) VALUES ('{2}')"
                                 .format(self.table, self.column, self.lE.text()))
                self.column_content = []
                self.db_read_column()

                self.cB.clear()
                self.cB.addItems(self.column_content)

                database.conn.commit()
                print self.column_content


class DelPositionFromCb:
    '''Class which is responsible for delete position from comboboxes'''
    def __init__(self, cB, cur, table, column):
        self.cB = cB
        self.cur = cur
        self.table = table
        self.column = column
        self.column_content = []

    def db_read_column(self):
        '''Method which read content of column in db table sort it and
        return '''
        self.cur.execute('SELECT {0} FROM {1}'.format(self.column, self.table))
        for x in self.cur:
            for y in x:
                self.column_content.append(x[y])
        self.column_content.sort()
        return self.column_content

    def reload_cb(self):
        '''Method which reload values in comboboxes'''
        self.column_content = []
        self.db_read_column()
        self.cB.clear()
        self.cB.addItems(self.column_content)

        #database.conn.commit()
        print self.column_content

    def del_position(self):
        '''Method which delates values form database table'''
        print self.cB.currentText()
        self.cur.execute('''DELETE FROM {0} WHERE "{1}" = '{2}';'''
                    .format(self.table, self.column, self.cB.currentText()))

        database.conn.commit()
        self.reload_cb()



class TableFromDb:
    '''Class which is responsible for connection to db by buildin PosgreSQL
    drivers in pyqt. It serves for displaying content form db to table view'''

    def __init__(self, dbname):
        try:
            self.qtdb = QtSql.QSqlDatabase.addDatabase('QPSQL')
            self.qtdb.setDatabaseName(dbname)
            self.qtdb.setHostName('localhost')
            self.qtdb.setPort(5433)
            self.qtdb.setUserName('postgres')
            self.qtdb.setPassword('postgres')
            self.qtdb.open()
            print "qtbd connected"
            self.model = QtSql.QSqlQueryModel()
            self.del_flag = None
            self.empty_current_item_flag = None

        except Exception as e:
            print e

    def initializeModel(self, item, lw_flag):
        '''Method which is responsible for querying db table'''

        if lw_flag == 1:

            self.model.setQuery(
                "SELECT {0}, {1}, {2}, {3}, {4} FROM {5} "
                "WHERE {6} = '{7}' ORDER BY {1}"
                .format('id', 'hour', 'week_day',
                        'class', 'teacher', 'reservation',
                        'classroom', item
                        ), self.qtdb)
            self.view.setColumnHidden(0, True)

            # if ui.listWidget.currentItem() is not None:
            #     ui.label_12.setText(
            #         _translate("Planer", ui.listWidget.currentItem().text(),
            #                    None))

            self.del_flag = 1

        elif lw_flag == 2:
            self.model.setQuery(
                "SELECT {0}, {1}, {2}, {3}, {4} FROM {5} "
                "WHERE {6} = '{7}' ORDER BY {1}"
                .format('id', 'hour', 'week_day',
                        'class', 'classroom', 'reservation',
                        'teacher', item
                        ), self.qtdb)
            self.view.setColumnHidden(0, True)

            # if ui.listWidget.currentItem() is not None:
            #     ui.label_12.setText(
            #         _translate("Planer", ui.listWidget2.currentItem().text(),
            #                    None))

            self.del_flag = 2
        elif lw_flag == 3:
            self.model.setQuery(
                "SELECT {0}, {1}, {2}, {3}, {4} FROM {5} "
                "WHERE {6} = '{7}' ORDER BY {1}"
                .format('id', 'hour', 'week_day',
                        'teacher', 'classroom', 'reservation',
                        'class', item
                        ), self.qtdb)
            self.view.setColumnHidden(0, True)

            # if ui.listWidget.currentItem() is not None:
            #     ui.label_12.setText(
            #         _translate("Planer", ui.listWidget3.currentItem().text(),
            #                    None))

            self.del_flag = 3

    def createView(self):
        '''Method which is responsible for table view which is added to
        main window'''
        self.view = QtGui.QTableView(Planer)
        self.view.setGeometry(QtCore.QRect(10, 321, 451, 211))

        self.view.setModel(self.model)
        self.view.horizontalHeader().setStretchLastSection(True)

        return self.view

    def delete_row(self):
        print self.del_flag
        index_list = []

        for model_index in self.view.selectionModel().selectedRows():

            index_list.append(int(model_index.data().toString()))

        print index_list
        for index in index_list:

            cur.execute("DELETE FROM {0} WHERE id = '{1}'".format('reservation'
                                                                  , index))
            database.conn.commit()
            self.table_fill()

        ui.fill_list_widget(ui.listWidget, 'classroom', ui.label_12)
        ui.class_table_count = ui.listWidget.count()
        print ui.class_table_count
        ui.fill_list_widget(ui.listWidget2, 'teacher', ui.label_12)
        ui.fill_list_widget(ui.listWidget3, 'class', ui.label_12)

    def table_fill(self):
        print self.del_flag
        if self.del_flag is None:
            lw_items = []
            for x in range(ui.listWidget.count()):
                lw_items.append(ui.listWidget.item(x))
            lw_text_items = []
            for x in lw_items:
                lw_text_items.append(x.text())
            if len(lw_text_items) >= 1:
                self.empty_current_item_flag = lw_text_items[0]
                print self.empty_current_item_flag
                self.initializeModel(self.empty_current_item_flag,
                                     1)
                # ui.label_12.setText(
                #         _translate("Planer", self.empty_current_item_flag,
                #                    None))

        if self.del_flag == 1:
            if ui.listWidget.currentItem() is None:

                self.initializeModel(self.empty_current_item_flag,
                                     1)
            else:
                self.initializeModel(ui.listWidget.currentItem().text(),
                                     1)
                self.empty_current_item_flag = ui.listWidget.currentItem().text()
        if self.del_flag == 2:
            if ui.listWidget2.currentItem() is None:
                self.initializeModel(self.empty_current_item_flag,
                                     2)
            else:
                self.initializeModel(ui.listWidget2.currentItem().text(),
                                     2)
                self.empty_current_item_flag = ui.listWidget2.currentItem().text()
        if self.del_flag == 3:
            if ui.listWidget3.currentItem() is None:
                self.initializeModel(self.empty_current_item_flag,
                                     3)
            else:
                self.initializeModel(ui.listWidget3.currentItem().text(),
                                     3)
                self.empty_current_item_flag = ui.listWidget3.currentItem().text()


if __name__ == "__main__":
    # try:

        import sys

        database = PlanerDb()
        cur = database.cur
        app = QtGui.QApplication(sys.argv)

        ui_login = Ui_Planer_login()
        Planer = QtGui.QDialog()
        ui_login.setupUi(Planer)

        if not Planer.exec_():  # 'reject': user pressed 'Cancel', so quit
            sys.exit()

        cur.execute('''SELECT "user", password from {0}
                   WHERE "user" = '{1}';'''
                    .format('admin_table', ui_login.lineEdit_user.text()))
        column = cur.fetchone()
        pass_db = base64.b64decode(column['password'])


        if ui_login.lineEdit_user.text() == 'admin' and pass_db == ui_login.lineEdit_password.text():
            cur = database.cur
            ui_admin = AdminWindow()
            Planer = QtGui.QMainWindow()
            ui_admin.setupUi(Planer)
            Planer.show()
            sys.exit(app.exec_())
        else:
            cur = database.cur
            ui = Ui_Planer()
            table = TableFromDb('db_' + ui_login.lineEdit_user.text())
            Planer = QtGui.QMainWindow()
            ui.setupUi(Planer)
            Planer.show()
            sys.exit(app.exec_())
    # except Exception as e:
    #     print e