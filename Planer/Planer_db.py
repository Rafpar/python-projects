__author__ = 'rafix'

import psycopg2
import psycopg2.extras
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
import base64

DATABASE = "postgres"
DATABASE_ADMIN = "db_admin"
DB_USER = "postgres"
DB_PASSWORD = "postgres"
DB_HOST = "localhost"
DB_PORT = "5433"
DB_HOURS_TABLE = "hours"
DB_CLASSES_TABLE = "classes"
DB_CLASSROOMS_TABLE = "classrooms"
DB_TEACHERS_TABLE = "teachers"
DB_RESERVATION_TABLE = "reservation"
DB_ADMIN_TABLE = "admin_table"
DB_LIST_FILE_PATH = "/home/rafix/db.txt"


class PlanerDb:

    def __init__(self):
        try:
            self.conn = psycopg2.connect(database=DATABASE,
                                         user=DB_USER,
                                         password=DB_PASSWORD,
                                         host=DB_HOST,
                                         port=DB_PORT,
                                         )
            print("Opened postgres database successfully")
            self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            self.cur = self.conn.cursor(cursor_factory=
                                        psycopg2.extras.RealDictCursor)
            try:
                if self.write_to_db_list_file(DATABASE_ADMIN) is True:
                    self.cur.execute('''CREATE DATABASE {0};'''
                                     .format(DATABASE_ADMIN))
                    self.conn.commit()
                    print ("admin database created successfully")

                    self.conn = psycopg2.connect(database=DATABASE_ADMIN,
                                                 user=DB_USER,
                                                 password=DB_PASSWORD,
                                                 host=DB_HOST,
                                                 port=DB_PORT,
                                                 )
                    print("Opened admin database successfully")
                    self.cur = self.conn.cursor(cursor_factory=
                                                psycopg2.extras.RealDictCursor)
                    self.create_admin_tables()
                else:

                    self.conn = psycopg2.connect(database=DATABASE_ADMIN,
                                                 user=DB_USER,
                                                 password=DB_PASSWORD,
                                                 host=DB_HOST,
                                                 port=DB_PORT,
                                                 )
                    print("Opened admin database successfully")
                    self.cur = self.conn.cursor(cursor_factory=
                                                psycopg2.extras.RealDictCursor)

            except Exception as e:
                print e

        except Exception as e:
            print("{0}".format(e))

    def create_admin_tables(self):
        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0} (id SERIAL PRIMARY
            KEY, "user" VARCHAR, password VARCHAR);'''.format(DB_ADMIN_TABLE))
        self.conn.commit()
        self.cur.execute('''SELECT * FROM {0} WHERE "user" = 'admin';'''.format(DB_ADMIN_TABLE))
        column = self.cur.fetchone()
        print column
        if column is not None:
            return
        else:
            self.cur.execute('''INSERT INTO {0} ("user", password) VALUES ('{1}', '{2}')'''.format(DB_ADMIN_TABLE, 'admin', base64.b64encode('admin')))
            self.conn.commit()

    def create_tables(self):

        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0} (id SERIAL PRIMARY
                        KEY, hour VARCHAR(50));'''.format(DB_HOURS_TABLE))
        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0} (id SERIAL PRIMARY
            KEY, class VARCHAR(50));'''.format(DB_CLASSES_TABLE))
        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0} (id SERIAL PRIMARY
            KEY, classroom VARCHAR(50));'''.format(DB_CLASSROOMS_TABLE))
        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0} (id SERIAL PRIMARY
            KEY, teacher VARCHAR(100));'''.format(DB_TEACHERS_TABLE))
        self.cur.execute('''CREATE TABLE IF NOT EXISTS {0}
                                        (id SERIAL PRIMARY KEY,
                                         hour VARCHAR(50),
                                         week_day VARCHAR(50),
                                         class VARCHAR(50),
                                         classroom VARCHAR(50),
                                         teacher VARCHAR(100));'''
                         .format(DB_RESERVATION_TABLE))
        self.conn.commit()
        print "tables for database created"

    def db_swich(self, db_name):
        try:
            self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            if self.write_to_db_list_file(db_name) is True:
                self.cur.execute(
                    '''CREATE DATABASE "{0}";'''.format(db_name))
                self.conn.commit()
                print ("{0} database created successfully".format(db_name))

                self.conn = psycopg2.connect(database=db_name,
                                             user=DB_USER,
                                             password=DB_PASSWORD,
                                             host=DB_HOST,
                                             port=DB_PORT,
                                             )
                print("Opened {0} database successfully".format(db_name))
                self.cur = self.conn.cursor(cursor_factory=
                                            psycopg2.extras.RealDictCursor)
                self.create_tables()
            else:

                self.conn = psycopg2.connect(database=db_name,
                                             user=DB_USER,
                                             password=DB_PASSWORD,
                                             host=DB_HOST,
                                             port=DB_PORT,
                                             )
                print("Opened {0} database successfully".format(db_name))
                self.cur = self.conn.cursor(cursor_factory=
                                            psycopg2.extras.RealDictCursor)

        except Exception as e:
            print e

    def db_delete(self, db_name):
        self.conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
        self.cur.execute(
            '''DROP DATABASE "{0}"'''.format(db_name))
        print 'database {0} has been deleted'.format(db_name)

        self.conn = psycopg2.connect(database=DATABASE_ADMIN,
                                     user=DB_USER,
                                     password=DB_PASSWORD,
                                     host=DB_HOST,
                                     port=DB_PORT,
                                     )
        print("Opened {0} database successfully".format('admin'))
        self.cur = self.conn.cursor(cursor_factory=
                                    psycopg2.extras.RealDictCursor)

    def write_to_db_list_file(self, database_name):
        db_list = []
        with open(DB_LIST_FILE_PATH, 'a+') as list_file:
            readlines = list_file.readlines()

            for line in readlines:
                db_list.append(line.strip())

            if database_name not in db_list:
                list_file.write(database_name + '\n')
                print "Database {0} name saved to the file"\
                    .format(database_name)
                return True
            else:
                print "{0} database already created".format(database_name)
                return False