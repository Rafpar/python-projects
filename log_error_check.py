__author__ = 'rafix'

import os
import binascii
from settings import *
from datetime import datetime, timedelta

ERR_INV_ERROR_LIST = "inv_error_list"


class ErrorChecker():
    """ErrorChecker catch all error lines appeared in logs in log directory
    and put them into the list. It also catch errors with non UTF-8 coding.
    Class should be instantiated with log object as an argument"""

    def __init__(self, log):
        self.log = log

    def error_check(self):
        output = ERR_INV_ERROR_LIST
        try:
            error_list = []
            new_error_list = []
            for f in os.listdir(VA_PATH + "/log"):
                if "." not in f[0]:
                    with open(VA_PATH + "/log/" + f, 'rb') as fi:
                        # counting from 1 to avoid pointing wrong line number
                        for i, line in enumerate(fi, 1):
                            try:
                                line.decode('utf-8')
                            except UnicodeDecodeError as e:
                                hexy = binascii.hexlify(line[e.start:])
                                line = str(line)
                                e_start = str(e.start)
                                self.log.debug('In line >>{0}<< in file >>{1}<'
                                               '< on position >>{2}<< on >>{3}'
                                               '<< non UTF-8 char detected. Pl'
                                               'ease gather current logs and i'
                                               'nform developers. Malformed li'
                                               'ne in hex form >>{4}<<'
                                               .format(i, f, e_start,
                                                       line[2:28], hexy))
                    with open(VA_PATH + "/log/" + f, 'r', encoding='latin-1') \
                            as fi:
                        for line in fi:
                            line_err = line[:41]
                            if "[ERROR   ]" in line_err:
                                error_list.append(line[7:])
                            elif "ERROR" in line_err:
                                error_list.append(line)
            error_list.sort()
            timediff = datetime.now() - timedelta(hours=ERROR_TIME_FRAME)
            for x in error_list:
                new_x = datetime.strptime(x[:19], '%Y-%m-%d %H:%M:%S')
                if new_x > timediff:
                    x = x.strip('\n')
                    new_error_list.append(x)
            if len(new_error_list) == 0:
                self.log.debug("Any error detected during last {0} hours"
                               .format(ERROR_TIME_FRAME))
            return new_error_list
        except ValueError as e:
            self.log.exception("{0}".format(e))
        except Exception:
            self.log.exception("unable to check logs")
        return output