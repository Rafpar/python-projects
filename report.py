__author__ = 'rafix'

import psycopg2
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders

DATABASE = "*****"
DB_USER = "***"
DB_PASSWORD = ""
DB_HOST = "localhost"
DB_PORT = "5432"
DB_INV_TABLE = "invoices"
DB_USER_TABLE = "users"
CSV_INV_PATH = ".../csv/invoices.csv"
CSV_USER_PATH = "...csv/users.csv"
REPORT_FILE_PATH = ".../csv/report.txt"


class ImportToDb():
    """This class is responsible for importing csv files to PostgreSQL,
     performing several analyses in database and creation of report which is
      send by email"""

    def create_invoices(self, cur):
        cur.execute('''DROP TABLE IF EXISTS {0}; CREATE TABLE {0}
                           (user_id INT8, amount DECIMAL(10,2),
                           created_at DATE);'''.format(DB_INV_TABLE))
        cur.execute("COPY {0} from '{1}' DELIMITER ','CSV HEADER"
                    .format(DB_INV_TABLE, CSV_INV_PATH))

    def create_users(self, cur):
        cur.execute('''DROP TABLE IF EXISTS {0}; CREATE TABLE {0} (id INT8,
                           name VARCHAR, email VARCHAR);'''
                    .format(DB_USER_TABLE))
        cur.execute("COPY {0} from '{1}' DELIMITER ','CSV HEADER"
                    .format(DB_USER_TABLE, CSV_USER_PATH))

    def queries(self, cur):
        with open(REPORT_FILE_PATH, 'w') as report:
            cur.execute("SELECT created_at, COUNT(*) "
                        "FROM invoices "
                        "GROUP BY created_at "
                        "ORDER BY created_at;")
            title1 = "Number of transactions per day"
            self.file_processing(cur, report, title1, line="")
            cur.execute("SELECT created_at, COUNT(DISTINCT user_id) "
                        "FROM invoices "
                        "GROUP BY created_at "
                        "ORDER BY created_at;")
            title2 = "Number of unique users performing transactions per day "
            self.file_processing(cur, report, title2, line="")
            cur.execute("SELECT split_part(email,'@',2) AS int_domain, "
                        "COUNT(*) "
                        "FROM users "
                        "GROUP BY  int_domain "
                        "ORDER BY int_domain;")
            title3 = "Number of unique users in particular domain"
            self.file_processing(cur, report, title3, line="line")
            cur.execute("SELECT user_id, COUNT(*) "
                        "FROM invoices "
                        "GROUP BY user_id "
                        "ORDER BY user_id ;")
            title4 = "Number of transactions per user"
            self.file_processing(cur, report, title4, line="line")
            cur.execute("SELECT user_id, COUNT(*) AS trans "
                        "FROM invoices "
                        "GROUP BY user_id "
                        "HAVING COUNT(*) > 3  "
                        "ORDER BY user_id;")
            title5 = "All users who have more than 3 transactions"
            self.file_processing(cur, report, title5, line="line")
            cur.execute("SELECT  AVG(aver.am), AVG(standard.st) "
                        "FROM "
                            "(SELECT AVG(amount) AS am "
                                "FROM invoices "
                                "GROUP BY created_at "
                                "ORDER BY created_at DESC LIMIT 7) AS aver, "
                            "(SELECT STDDEV(amount) AS st "
                                "FROM invoices "
                                "GROUP BY created_at "
                                "ORDER BY created_at DESC LIMIT 7) AS standard"
                        )
            title6 = "Mean value of transactions and standard deviation from " \
                     "last 7 days"
            self.file_processing(cur, report, title6, line="line")

    def file_processing(self, cur, report, title, line):
        report.write("--------------------{0}-----------------------------\n"
                     .format(title))
        report.write("####################################################"
                     "############################################\n")
        fetch = cur.fetchall()
        for x in fetch:
            for y in x:
                report.write(str(y) + " ")
            if line == "line":
                report.write("\n")
            else:
                continue
        report.write("####################################################"
                     "############################################\n")

    def run_database(self):
        try:
            conn = psycopg2.connect(database=DATABASE,
                                    user=DB_USER,
                                    password=DB_PASSWORD,
                                    host=DB_HOST,
                                    port=DB_PORT)
            print("Opened database successfully")
            cur = conn.cursor()
            self.create_invoices(cur)
            self.create_users(cur)
            self.queries(cur)
            conn.commit()
            print("database successfully updated")
            conn.close()
            print("connection closed")
            self.send_email()
        except Exception as e:
            print("{0}".format(e))

    def send_email(self):
        try:
            fromaddr = "*****"
            toaddr = "****"
            msg = MIMEMultipart()
            msg['From'] = fromaddr
            msg['To'] = toaddr
            msg['Subject'] = "Avaus report"
            body = "Report in attachment"
            msg.attach(MIMEText(body, 'plain'))
            filename = "report.txt"
            attachment = open(".../csv/report.txt",
                              "rb")
            part = MIMEBase('application', 'octet-stream')
            part.set_payload((attachment).read())
            encoders.encode_base64(part)
            part.add_header('Content-Disposition', "attachment; filename= %s" %
                            filename)
            msg.attach(part)
            server = smtplib.SMTP('smtp.gmail.com', 587)
            server.starttls()
            server.login(fromaddr, "********")
            text = msg.as_string()
            server.sendmail(fromaddr, toaddr, text)
            server.quit()
            print("email sent")
        except Exception as e:
            print("Email sending error , {0}".format(e))

importdb = ImportToDb()
importdb.run_database()

