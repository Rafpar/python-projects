__author__ = 'rafix'

import paho.mqtt.publish as publish
import json
import git

COMMIT_PATH = PATH + "/log/last_commit.log"
AWS_MQTT_TOPIC_UPDATE_PREFIX = '$aws/things/'


class State_Update():
    """State_Update class is responsible for sending current state of a
    device"""

    def __init__(self, mqttc, log, id):
        self.mqttc = mqttc
        self.log = log
        self.id = id

    def state(self):
        try:
            g = git.Git(VA_PATH)
            current_hash = g.log('-n', '1', '--pretty=format:%H')
            j = {
                "state": {
                    "reported": {
                        "hash": current_hash
                    }
                }
            }
            data = json.dumps(j)
            with open(COMMIT_PATH, 'w') as commit:
                commit.write(current_hash)
            self.mqttc.publish(AWS_MQTT_TOPIC_UPDATE_PREFIX + self.id +
                               "/shadow/update", payload=data, qos=1)
            self.log.info("Current hash reported as {0}"
                          .format(current_hash))
        except:
            self.log.exception("problem with commit state")